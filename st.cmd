#!/usr/bin/env iocsh.bash

################################################################
### requires
require(mrfioc2)
require(evr_seq_calc)


epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")
epicsEnvSet("IOC", "utg-evr-02")
epicsEnvSet("SYS", "LabS-Utgard-VIP:")
epicsEnvSet("EVR", "EVR-2")
epicsEnvSet("DEV", "TS-$(EVR)")
epicsEnvSet("SYSPV", "$(SYS)$(DEV)")
epicsEnvSet("PCIID", "1:0.0")
epicsEnvSet("CHIC_SYS", "$(SYS)")
epicsEnvSet("CHOP_DRV", "Chop-Drv-01")
epicsEnvSet("MRF_HW_DB", "evr-pcie-300dc-univ.db")
epicsEnvSet("BASEEVTNO", "21")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
# E3 Common databases
iocshLoad("$(essioc_DIR)/essioc.iocsh")

#iocshLoad("$(mrfioc2_DIR)/evr.iocsh", "EVRDB=$(MRF_HW_DB), P=$(P), EVR=$(EVR), PCIID=$(PCIID)")
iocshLoad("$(mrfioc2_DIR)/evr.iocsh", "EVRDB=$(MRF_HW_DB), P=$(SYSPV), EVR=EVR, PCIID=$(PCIID)")

#iocshLoad("$(mrfioc2_DIR)/evrevt.iocsh", "P=$(P), R=$(R), EVR=$(EVR)")
iocshLoad("$(mrfioc2_DIR)/evrevt.iocsh", "P=$(SYSPV)")

dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R=-00, S=":", EVR=EVR, CODE=27, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")
#dbLoadRecords("mrmevrtsbuf-univ.db","P=$(P), R=$(R)-01:, S="", EVR=$(EVR), CODE=28, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")
#dbLoadRecords("mrmevrtsbuf-univ.db","P=$(P), R=$(R)-02:, S="", EVR=$(EVR), CODE=29, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")
#dbLoadRecords("mrmevrtsbuf-univ.db","P=$(P), R=$(R)-02:, S="", EVR=$(EVR), CODE=30, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")


# Load the sequencer configuration script
# Load the sequencer configuration script
iocshLoad("$(evr_seq_calc_DIR)/evr_seq_calc.iocsh", "P=$(SYS), R=$(CHOP_DRV), EVR=$(DEV):, BASEEVTNO=$(BASEEVTNO)")

iocInit()

#iocshLoad("$(mrfioc2_DIR)/evr.r.iocsh", "P=$(SYSPV), R=$(R), EVR=$(EVR)")
iocshLoad("$(mrfioc2_DIR)/evr.r.iocsh", "P=$(SYSPV), EVR=EVR")

######### INPUTS #########
## Set up of UnivIO 0 as Input. Generate Code 10 locally on rising edge.
#dbpf $(SYSPV):In0-Lvl-Sel "Active High"
#dbpf $(SYSPV):In0-Edge-Sel "Active Rising"
#dbpf $(SYSPV):OutFPUV00-Src-SP 61
#dbpf $(SYSPV):In0-Trig-Ext-Sel "Off"
#dbpf $(SYSPV):In0-Code-Ext-SP 20
#
## Set up of UnivIO 1 as Input. Generate Code 11 locally on rising edge.
#dbpf $(SYSPV):In1-Lvl-Sel "Active High"
#dbpf $(SYSPV):In1-Edge-Sel "Active Rising"
#dbpf $(SYSPV):OutFPUV01-Src-SP 61
#dbpf $(SYSPV):In1-Trig-Ext-Sel "Off"
#dbpf $(SYSPV):In1-Code-Ext-SP 21
#
## Set up of UnivIO 1 as Input. Generate Code 12 locally on rising edge.
#dbpf $(SYSPV):In2-Lvl-Sel "Active High"
#dbpf $(SYSPV):In2-Edge-Sel "Active Rising
#dbpf $(SYSPV):OutFPUV02-Src-SP 61
#dbpf $(SYSPV):In2-Trig-Ext-Sel "Off"
#dbpf $(SYSPV):In2-Code-Ext-SP 22
#
## Set up of UnivIO 1 as Input. Generate Code 13 locally on rising edge.
#dbpf $(SYSPV):In3-Lvl-Sel "Active High"
#dbpf $(SYSPV):In3-Edge-Sel "Active Rising"
#dbpf $(SYSPV):OutFPUV03-Src-SP 61
#dbpf $(SYSPV):In3-Trig-Ext-Sel "Off"
#dbpf $(SYSPV):In3-Code-Ext-SP 23


# Trig-Ext-Sel changed from "Off" to "Edge", Code-Ext-SP changed from 0 to 10
dbpf $(SYSPV):UnivIn-0-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-0-Edge-Sel "Active Rising"
#dbpf $(SYSPV):OutFPUV00-Src-SP 61
dbpf $(SYSPV):Out-RB00-Src-SP 61
dbpf $(SYSPV):UnivIn-0-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-0-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-0-Code-Ext-SP 25
dbpf $(SYSPV):UnivIn-0-Code-Back-SP 0

dbpf $(SYSPV):UnivIn-1-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-1-Edge-Sel "Active Rising"
#dbpf $(SYSPV):OutFPUV01-Src-SP 61
dbpf $(SYSPV):Out-RB01-Src-SP 61
dbpf $(SYSPV):UnivIn-1-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-1-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-1-Code-Ext-SP 21
dbpf $(SYSPV):UnivIn-1-Code-Back-SP 0

dbpf $(SYSPV):UnivIn-2-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-2-Edge-Sel "Active Rising"
#dbpf $(SYSPV):OutFPUV02-Src-SP 61
dbpf $(SYSPV):Out-RB02-Src-SP 61
dbpf $(SYSPV):UnivIn-2-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-2-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-2-Code-Ext-SP 22
dbpf $(SYSPV):UnivIn-2-Code-Back-SP 0

dbpf $(SYSPV):UnivIn-3-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-3-Edge-Sel "Active Rising"
#dbpf $(SYSPV):OutFPUV03-Src-SP 61
dbpf $(SYSPV):Out-RB03-Src-SP 61
dbpf $(SYSPV):UnivIn-3-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-3-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-3-Code-Ext-SP 23
dbpf $(SYSPV):UnivIn-3-Code-Back-SP 0

dbpf $(SYSPV):EvtA-SP.OUT "@OBJ=EVR,Code=14" 
dbpf $(SYSPV):EvtA-SP.VAL 14 
dbpf $(SYSPV):EvtB-SP.OUT "@OBJ=EVR,Code=21" 
dbpf $(SYSPV):EvtB-SP.VAL 21 
dbpf $(SYSPV):EvtC-SP.OUT "@OBJ=EVR,Code=22" 
dbpf $(SYSPV):EvtC-SP.VAL 22
dbpf $(SYSPV):EvtD-SP.OUT "@OBJ=EVR,Code=23"
dbpf $(SYSPV):EvtD-SP.VAL 23
dbpf $(SYSPV):EvtE-SP.OUT "@OBJ=EVR,Code=24"
dbpf $(SYSPV):EvtE-SP.VAL 24
dbpf $(SYSPV):EvtF-SP.OUT "@OBJ=EVR,Code=25"
dbpf $(SYSPV):EvtF-SP.VAL 25
dbpf $(SYSPV):EvtG-SP.OUT "@OBJ=EVR,Code=26"
dbpf $(SYSPV):EvtG-SP.VAL 26
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=EVR,Code=27"
dbpf $(SYSPV):EvtH-SP.VAL 27
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=EVR,Code=28"
dbpf $(SYSPV):EvtH-SP.VAL 28
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=EVR,Code=29"
dbpf $(SYSPV):EvtH-SP.VAL 29
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=EVR,Code=30"
dbpf $(SYSPV):EvtH-SP.VAL 30



######### OUTPUTS #########
#Set up delay generator 0 to trigger on event 14
dbpf $(SYSPV):DlyGen-0-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-0-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-0-Evt-Trig0-SP 14

#Set up delay generator 1 to trigger on event 14
dbpf $(SYSPV):DlyGen-1-Evt-Trig0-SP 14
dbpf $(SYSPV):DlyGen-1-Width-SP 2860 #1ms
dbpf $(SYSPV):DlyGen-1-Delay-SP 0 #0ms
#dbpf $(SYSPV):OutFPUV04-Src-SP 2 #Connect output2 to DlyGen--2
dbpf $(SYSPV):Out-RB04-Src-SP 1

#Set up delay generator 2 to trigger on event 17
dbpf $(SYSPV):DlyGen-2-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-2-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-2-Evt-Trig0-SP 17

#Set up delay generator 3 to trigger on event 18
dbpf $(SYSPV):DlyGen-3-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-3-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-3-Evt-Trig0-SP 18
#dbpf $(SYSPV):OutFPUV05-Src-SP 3 #Connect output3 to DlyGen--3
dbpf $(SYSPV):Out-RB05-Src-SP 3

######## Sequencer #########
dbpf $(SYSPV):EndEvtTicks 4

# Load sequencer setup
dbpf $(SYSPV):SoftSeq-0-Load-Cmd 1

# Enable sequencer
dbpf $(SYSPV):SoftSeq-0-Enable-Cmd 1

# Select run mode, "Single" needs a new Enable-Cmd every time, "Normal" needs Enable-Cmd once
dbpf $(SYSPV):SoftSeq-0-RunMode-Sel "Normal"


# Use ticks or microseconds
dbpf $(SYSPV):SoftSeq-0-TsResolution-Sel "Ticks"

# Select trigger source for soft seq 0, trigger source 0, delay gen 0
dbpf $(SYSPV):SoftSeq-0-TrigSrc-0-Sel 0

# Commit all the settings for the sequnce
# commit-cmd by evrseq!!! 
dbpf $(SYSPV):SoftSeq-0-Commit-Cmd "1"

